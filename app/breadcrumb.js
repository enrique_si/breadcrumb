angular.module('breadcrumbApp', [])
  .controller('BreadcrumbController', function() {

    var breadcrumb = this;

    breadcrumb.atcLevels = [
      {level:0, elements:['A','B','C','D','E'], selected:""}
    ];

    breadcrumb.addLevel = function(atcLevelSelected){

      if(atcLevelSelected.level > 5 ) return;

      var newAtcLevels = [];

      angular.forEach(breadcrumb.atcLevels, function(atcLevel) {

        if ( atcLevel.level <= atcLevelSelected.level ) newAtcLevels.push(atcLevel)

      });

      breadcrumb.atcLevels = newAtcLevels ;

      breadcrumb.atcLevels.push({
        level: atcLevelSelected.level+1,
        elements:[
          atcLevelSelected.selected+'A',
          atcLevelSelected.selected+'B',
          atcLevelSelected.selected+'C',
          atcLevelSelected.selected+'D',
          atcLevelSelected.selected+'E'
        ]
      });
    };

  });
